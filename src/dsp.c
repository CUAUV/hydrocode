#include "main.h"
#include "arm_math.h" 
#include "arm_const_structs.h"
#include "dsp.h"
#include "preprocessor_assert.h"
#include <complex.h>
 

static float cmplx_ch_A[FFT_LENGTH*2];
static float cmplx_ch_B[FFT_LENGTH*2];
static float cmplx_ch_C[FFT_LENGTH*2];

#define cmplx_ch_0_0 cmplx_ch_B
#define cmplx_ch_0_1 cmplx_ch_C
#define cmplx_ch_1_0 cmplx_ch_A



static float rfft_mag[FFT_LENGTH];
float PSD_THRESHOLD = .1;//.92400f;
uint32_t COOLDOWN_SAMPLES = 350000;
static int64_t samples_received_since_boot = 0;
static int64_t num_samples_at_last_ping = -400000; 

typedef struct principal_frequency_t
{
    int index;
    //float frequency;
    float psd;
}principal_frequency;
principal_frequency calculate_pf(float * arr_in, int search_length);
float phase_difference(float phase_a, float phase_b);
uint8_t detect_heading(uint16_t * fixed_precision_input,heading_info_t * heading) 
{ 
  unsigned int i,k;


  for(i = 0; i < SINGLE_CHANNEL_BUFFERSIZE; i+= FFT_LENGTH)
  {
      for(k = 0; k < FFT_LENGTH; k++)
      {
      cmplx_ch_A[2*k] = (float)fixed_precision_input[3*k+0]; //Convert uint16_ts to floats.
      cmplx_ch_A[2*k+1] = 0.0f;
      cmplx_ch_B[2*k] = (float)fixed_precision_input[3*k+1]; //Convert uint16_ts to floats.
      cmplx_ch_B[2*k+1] = 0.0f;
      cmplx_ch_C[2*k] = (float)fixed_precision_input[3*k+2]; //Convert uint16_ts to floats.
      cmplx_ch_C[2*k+1] = 0.0f;
      }
      CASSERT(FFT_LENGTH==64,dsp_c); //IF THIS ASSERT FAILS IT MEANS THAT YOU MUST TWEAK arm_cfft_sR_f32_lenXXXX such that XXXX=FFT_LENGTH
      arm_cfft_f32(&arm_cfft_sR_f32_len64, cmplx_ch_0_0, 0, 1);
      principal_frequency pf = calculate_pf(cmplx_ch_0_0,FFT_LENGTH);
      if((pf.psd >= PSD_THRESHOLD) && (samples_received_since_boot - num_samples_at_last_ping) >= COOLDOWN_SAMPLES)
      //if((samples_received_since_boot - num_samples_at_last_ping) >= COOLDOWN_SAMPLES)
      {
       arm_cfft_f32(&arm_cfft_sR_f32_len64, cmplx_ch_0_1, 0, 1);
       arm_cfft_f32(&arm_cfft_sR_f32_len64, cmplx_ch_1_0, 0, 1);

       float complex dft_term_0_0 = cmplx_ch_0_0[pf.index] + cmplx_ch_0_0[pf.index+1]*I;
       float complex dft_term_0_1 = cmplx_ch_0_1[pf.index] + cmplx_ch_0_1[pf.index+1]*I;
       float complex dft_term_1_0 = cmplx_ch_1_0[pf.index] + cmplx_ch_1_0[pf.index+1]*I;

       float angle_0_0 = atan2(cimagf(dft_term_0_0),crealf(dft_term_0_0));
       float angle_0_1 = atan2(cimagf(dft_term_0_1),crealf(dft_term_0_1));
       float angle_1_0 = atan2(cimagf(dft_term_1_0),crealf(dft_term_1_0));

       float diffY = phase_difference(angle_0_1,angle_0_0);
       float diffX = phase_difference(angle_1_0,angle_0_0);
       float heading_angle = atan2(diffY,diffX);
       num_samples_at_last_ping = samples_received_since_boot;
       /*
		uint32_t maxIndex = 0;
		float maxValue;
  		arm_cmplx_mag_squared_f32(cmplx_ch_0_0, rfft_mag, FFT_LENGTH);
        rfft_mag[0] = 0.0f;
  		arm_max_f32(rfft_mag, FFT_LENGTH, &maxValue, &maxIndex);
        float tot_pow = 0.0f;
        for(int f = 0; f < FFT_LENGTH; f++)
        {
            tot_pow += rfft_mag[f];
        }
        */
       heading->azimuth_radians = heading_angle; 
       heading->trigger_psd = pf.psd;
       heading->frequency = (SAMPLE_RATE/FFT_LENGTH/2.0) * pf.index; 
       heading->zenith_radians = pf.index;
       return 1;
      }
   samples_received_since_boot += FFT_LENGTH;
         
  }
 
 return 0;
}
float phase_difference(float phase_a, float phase_b)
{
    float diff = phase_a - phase_b;
    while(diff >= M_PI)
    {
        diff -= 2.0f*M_PI;
    }
    while(diff < -1*M_PI)
    {
        diff += 2.0f*M_PI;
    }
    return diff;
}
principal_frequency calculate_pf(float * arr_in, int search_length)
{
    float mag_sq_sum = 0;
    float mag_sq_max = 0;
    int max_idx = 0;

    int i;
    for(i=(IGNORE_DC_COMPONENT) ? 2 : 0; i < search_length; i+=2)
    {
        float sq_mag = (arr_in[i] * arr_in[i]) + (arr_in[i+1] * arr_in[i+1]);
        mag_sq_sum += sq_mag;
        if(sq_mag > mag_sq_max)
        {
            max_idx = i;
            mag_sq_max = sq_mag;
        }
    }
    principal_frequency pf;
    pf.index = max_idx;
    //pf.frequency = SAMPLE_RATE*(index/FFT_LENGTH); 
    pf.psd = mag_sq_max/mag_sq_sum;
    return pf;
}
float inline sum_vec(float* arr, int blocksize)
{
    float acc = 0;
    int t;
    for(t = 0; t < blocksize; t++)
    {
      acc += arr[t]; 
    }
    return acc;
}
float circular_var(float* real_vec, float* imag_vec, int blocksize)
{
float cvar = 0;

float real_sum = sum_vec(real_vec,blocksize);
float imag_sum = sum_vec(imag_vec,blocksize);

float r = sqrt(real_sum*real_sum + imag_sum*imag_sum);

float r_normalized = r/blocksize;
//float cos_sumA = sum_vec(track_history_cos_chA,PHASE_BUFFER_LEN);
return 1.0f-r_normalized;
}
