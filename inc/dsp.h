

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DSP_H
#define __DSP_H

#ifdef __cplusplus
 extern "C" {
#endif

#define PHASE_BUFFER_LEN 32
#define FFT_LENGTH 64 
static int IGNORE_DC_COMPONENT = 1;

typedef enum {TRACK=0,SEARCH=1} DSP_MODE; 
void init_rfft();

void set_psd_threshold(float thresh);
void set_cooldown_samples(uint32_t cooldown_samples);

typedef struct heading_information{
    float azimuth_radians;
    float zenith_radians;
    float trigger_psd; 
    float frequency;
} heading_info_t;

uint8_t detect_heading(uint16_t * fixed_precision_input,heading_info_t * heading) ;

extern float PSD_THRESHOLD;
extern uint32_t COOLDOWN_SAMPLES;
#ifdef __cplusplus
}
#endif

#endif /* __DSP_H */



