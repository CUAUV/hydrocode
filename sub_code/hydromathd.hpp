#ifndef __HYDROMATHD_HPP__
#define __HYDROMATHD_HPP__

#include <iostream>
#include <cstdint>
#include <ctime>
#include "kiss_fft.h"
#include "udp_receiver.h"
#include <cmath>
#include <complex>
#include <thread>
#include "libshm/c/shm.h"

#define buffer_0_0 buffer_B
#define buffer_0_1 buffer_C
#define buffer_1_0 buffer_A

struct hydrophones_settings shm_settings;
struct hydrophones_results_spectrum shm_results_spectrum;
struct hydrophones_results_track shm_results_track;

const int SAMPLING_FREQUENCY = 400000;
const int SAMPLING_DEPTH = 128;
const int SPECTRUM_FFT_LENGTH = 1024; //Must be a power of 2
const int TRACK_FFT_LENGTH = 256;

double prev_psd = 0;
long current_sample_count = 0;
long old_track_sample_count = 0;
long old_spectrum_sample_count = 0;

//Direction_thread --> using IIR
void direction_loop();


//Spectrum_thread --> using FFT (kiss?)
void spectrum_loop();


#endif //__HYDROMATHD_HPP__
