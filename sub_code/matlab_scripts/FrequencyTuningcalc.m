clear
load('../cython_track/pool_180deg.mat')

FS = 400e3;
NFFT=256;
SEARCH_STEP_SIZE = 32;
PSD_THRESH = .6;
COOLDOWN_SAMPLES = 350e3;

samples_0_0 = ping_B;
samples_0_1 = ping_C;
samples_1_0 = ping_A;
last_high_psd_idx = -1e6; %Arbitrary large negative number on start
ping_mask = zeros(1,length(samples_0_0));
ping_start_mask = zeros(1,length(samples_0_0));
%hamming_window = hamming(256)';
window =  hann(NFFT)';%kaiser(NFFT,5)';
psds = [];
for k = 1:SEARCH_STEP_SIZE:length(samples_0_0)-NFFT
    t=1:NFFT;
    yin_0_0 = double(samples_0_0(k:k+NFFT-1));
    yin_0_0 = yin_0_0 - mean(yin_0_0);
    yin_0_0 = yin_0_0.*window;
    yin_0_1 = double(samples_0_1(k:k+NFFT-1));
    yin_0_1 = yin_0_1 - mean(yin_0_1);
    yin_0_1 = yin_0_1.*window;
    yin_1_0 = double(samples_1_0(k:k+NFFT-1));
    yin_1_0 = yin_1_0 - mean(yin_1_0);
    yin_1_0 = yin_1_0.*window;

    mag_sq = abs(fft(yin_0_0)).^2;
    mag_sq(1) = 0; % set DC component to zero
    mag_sq_half_spectrum = mag_sq(2:ceil(length(mag_sq)/2));
    total_power = sum(mag_sq_half_spectrum);
    psd = max(mag_sq_half_spectrum) / total_power;
    psds = [psds psd];
%         semilogy(linspace(0,200e3,length(mag_sq_half_spectrum)),mag_sq_half_spectrum);
%         title('Power Spectrum of Signal Measured at Transducer','FontSize',14);
%         xlabel('Frequency (Hz)','FontSize',14);
%         ylabel('Power','FontSize',14);
%         ylim([10^2 10^9])
%         grid on;
%         pause
    if psd>PSD_THRESH

        ping_mask(k:k+NFFT-1) = ones(1,NFFT);
        ping_freq_idx = find(max(mag_sq_half_spectrum) == mag_sq_half_spectrum);
        if k-last_high_psd_idx >= COOLDOWN_SAMPLES %This is the leading edge of a ping.
            last_high_psd_idx = k;
            ping_start_mask(k:k+NFFT-1) = ones(1,NFFT);
            fprintf('PING FREQ is %f\n', (400e3/NFFT)*ping_freq_idx);
            spectrum_0_1 = fft(yin_0_0);
            spectrum_0_0 = fft(yin_0_1);
            spectrum_1_0 = fft(yin_1_0);

            angle_0_1_at_ping_freq = angle(spectrum_0_1(ping_freq_idx+1));
            angle_0_0_at_ping_freq = angle(spectrum_0_0(ping_freq_idx+1));
            angle_1_0_at_ping_freq = angle(spectrum_1_0(ping_freq_idx+1));
            
            diff0 = phase_difference(angle_0_1_at_ping_freq,angle_0_0_at_ping_freq);
            diff1 = phase_difference(angle_1_0_at_ping_freq,angle_0_0_at_ping_freq);

            heading = atan2(diff0,diff1);
            fprintf('@index=%i, HEADING: %f\n',k, heading*180/pi);
            fprintf('MAX elem: %f. Total %f',max(mag_sq_half_spectrum),total_power);
        end
    end
    
end
n = 1:length(samples_0_0);
subplot(3,1,1);
[hAx,hLine1,hLine2] = plotyy(n,samples_0_0,[n',n'],[ping_mask',ping_start_mask']);
set(hAx(2),'ylim',[-.25 1.25]);
title('Samples and Mask');

subplot(3,1,2);
plot(1:length(psds),psds);
title('Power Spectral Density vs \lambda');
xlabel('\lambda');
ylabel('PSD max ratio');
subplot(3,1,3);
plot(1:NFFT,window);
title('Window');
xlim([0 NFFT]);