function [ out ] = phase_difference( a,b )
    out = a - b;
    while out > pi
        out = out - 2*pi;
    end
    while out < -pi
        out = out + 2*pi;
    end
end

