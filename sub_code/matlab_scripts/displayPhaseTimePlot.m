clear
load('log_lab.mat')
load('lab_33khz_split.mat')

n = 1:length(ping_A);

subplot(2,1,1);
plot(n,dtft_0_0_phase, n, dtft_1_0_phase, n, dtft_0_1_phase);
title('Phase vs time')
legend('0_0','1_0','0_1');
subplot(2,1,2);
title('Actual data')
plot(n,ping_A,n,ping_B,n,ping_C)

