
#include "udp_receiver.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <iterator>
#include <matio.h>
#include <stdint.h>
using namespace std;

//const int MAX_VEC_LEN = 100000;
#define MAX_VEC_DURATION 20
#define SAMPLE_RATE 400000
//const long MAX_VEC_LEN = CHANNEL_DEPTH*3*SAMPLE_RATE*MAX_VEC_DURATION;
//const long MAX_VEC_LEN = 460800000;
//uint16_t vec[MAX_VEC_LEN];
int line_count;
const int64_t *ping_A_data;
const int64_t *ping_B_data;
const int64_t *ping_C_data;
unsigned ping_length; 



void init()
{
    mat_t *mat = Mat_Open(fname.c_str(),MAT_ACC_RDONLY);
    if(!mat)
    {
        cerr << "COULD NOT OPEN MAT" << endl;
        exit(1);
    }
    matvar_t *ping_A_var = 0;
    matvar_t *ping_B_var = 0;
    matvar_t *ping_C_var = 0;
    ping_A_var = Mat_VarRead(mat,(char*)"ping_A");
    ping_B_var = Mat_VarRead(mat,(char*)"ping_B");
    ping_C_var = Mat_VarRead(mat,(char*)"ping_C");

    if(!ping_A_var || !ping_B_var || !ping_C_var)
    {
        cerr<< "COULD NOT FIND A VARIABLE"<<endl;
        exit(1);
    }
    ping_A_data = static_cast<const int64_t*>(ping_A_var->data);
    ping_B_data = static_cast<const int64_t*>(ping_B_var->data);
    ping_C_data = static_cast<const int64_t*>(ping_C_var->data);
    ping_length = ping_A_var->nbytes/ping_A_var->data_size;
    line_count = ping_length/CHANNEL_DEPTH;

}
int loop(superdongle_packet_t * buffer ) {
  static int count = 0;
  if(!count)
  {
    init();
    cout << "Init done" << endl;
  }
  if(count < line_count)
  {
  
      for(int i = 0; i < CHANNEL_DEPTH; i++)
      {
          buffer->data[3*i] = ping_A_data[CHANNEL_DEPTH*count + i]; 
          buffer->data[3*i + 1] = ping_B_data[CHANNEL_DEPTH*count +i];
          buffer->data[3*i + 2] = ping_C_data[CHANNEL_DEPTH*count +i];
      }
  count++;
  return 0;
  }
  return 1;
}
