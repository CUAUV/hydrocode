#include "hydromathd.hpp"
#include "track_iir.hpp"
#include "liquid.h"//also installed fftw.hpp for faster fft (inherited dependency)

//received UDP packet from uC
superdongle_packet_t spt;
windowcf w;

windowcf wchA;
windowcf wchB;
windowcf wchC;

iirfilt_crcf track_filter;
nco_crcf nco;
FilterFactory ff;
int local_track_freq = -1;
/*
double phase_difference(double phase_a, double phase_b);

*/
std::complex<float> spectrum_fft_output [SPECTRUM_FFT_LENGTH];
float spectrum_fft_magnitude [SPECTRUM_FFT_LENGTH/2];


void do_track(){
  
  if(local_track_freq != shm_settings.track_frequency_target) 
  {
    if(ff.sosMap.count(shm_settings.track_frequency_target)) //frequency key exists
    {
      if (track_filter != NULL)
      {
        iirfilt_crcf_destroy(track_filter);
      }

      float * b = &((ff.sosMap[shm_settings.track_frequency_target]->b)[0]);
      float * a = &((ff.sosMap[shm_settings.track_frequency_target]->a)[0]);
      track_filter = iirfilt_crcf_create_sos(b,a,NUMBER_OF_SECTIONS);
      shm_results_track.track_state = 0;
      local_track_freq = shm_settings.track_frequency_target;
      float normalized_frequency = (float) shm_settings.track_frequency_target / (float) SAMPLING_FREQUENCY;
      nco_crcf_set_frequency(nco,2*M_PI*normalized_frequency);
    }
    else //frequency key does not exist. Trigger error state
    {
      shm_results_track.track_state = -1;
    }
  }
  for(int i = 0; i < SAMPLING_DEPTH; i++)
  {
  
  //iirfilt_crcf_execute(track_filter,
  }
  shm_setg(hydrophones_results_track, shm_results_track);
}



void do_spectrum(){
  
  std::complex<float> * data_in;
  windowcf_read(w,&data_in);
  fftplan q = fft_create_plan(SPECTRUM_FFT_LENGTH,data_in,spectrum_fft_output,LIQUID_FFT_FORWARD,0);

  fft_execute(q);
  fft_destroy_plan(q);
  
  spectrum_fft_magnitude[0] = 0.0; //Do not consider DC comp
  
  int maxIdx = 0;
  float maxVal = 0;
  double total_power = 0;
  double psd = 0;
  for(int i = 1; i < SPECTRUM_FFT_LENGTH/2; i++)
  {
    spectrum_fft_magnitude[i] = std::norm(spectrum_fft_output[i]);
    total_power += spectrum_fft_magnitude[i];
    if (spectrum_fft_magnitude[i] > maxVal)
    {
    	maxIdx = i;
    	maxVal = spectrum_fft_magnitude[i];
    } 
  }
  psd = maxVal/total_power;
  
  if(shm_settings.auto_magnitude_thresholding == 0)
  {
    if(psd > shm_settings.spectrum_magnitude_threshold && (current_sample_count - old_spectrum_sample_count > shm_settings.spectrum_cooldown_samples))
    {
    	  shm_results_spectrum.most_recent_ping_magnitude = maxVal;
  	  shm_results_spectrum.most_recent_ping_frequency = (double) SAMPLING_FREQUENCY / (double) SPECTRUM_FFT_LENGTH * (double) maxIdx;
          shm_results_spectrum.most_recent_ping_count++;
          old_spectrum_sample_count = current_sample_count;
    }
    prev_psd = psd;
  }
  else{
    //IMPLEMENTE AUTO THRESHOLDING AND ASSOCIATED PING STUFF
  } 
  shm_setg(hydrophones_results_spectrum, shm_results_spectrum);
}


int main (void) {
  w=windowcf_create(SPECTRUM_FFT_LENGTH);
  wchA = windowcf_create(TRACK_FFT_LENGTH);
  wchB = windowcf_create(TRACK_FFT_LENGTH);
  wchC = windowcf_create(TRACK_FFT_LENGTH);

  nco = nco_crcf_create(LIQUID_NCO);
  std::cout << "hydromath daemon is beginning..." << std::endl;
  shm_init();
  
  //std::thread track_thread(direction_loop);
  //std::thread spectrum_thread(spectrum_loop);
  
  while(loop(&spt) == 0)
  {
    shm_getg(hydrophones_settings, shm_settings);
    for(int i = 0; i < 3*CHANNEL_DEPTH; i+=3)
    {
        windowcf_push(w,std::complex<float> (spt.data[i+1],0)); //This uses channel B
        windowcf_push(wchA, std::complex<float> (spt.data[i],0));
        windowcf_push(wchB, std::complex<float> (spt.data[i+1],0));
        windowcf_push(wchC, std::complex<float> (spt.data[i+2],0));
    }
    current_sample_count+=CHANNEL_DEPTH;        
    do_spectrum();
  } 
  
  printf("loop done %li =ci\n",current_sample_count/CHANNEL_DEPTH);
  return 0;
}



