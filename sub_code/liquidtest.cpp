#include "hydromathd.hpp"
#include "track_iir.hpp"
#include "liquid.h"//also installed fftw.hpp for faster fft (inherited dependency)
#include <iostream>
#include <iomanip>
#define _USE_MATH_DEFINES
#include <cmath>
#include <complex>
windowcf w;
iirfilt_crcf track_filter;
nco_crcf nco;
FilterFactory ff;
const int Fs = 400000;
const int tft = 40000.0;
int main (void) {

    //float * b = &((ff.sosMap[tft]->b)[0]);
    //float * a = &((ff.sosMap[tft]->a)[0]);
    //track_filter = iirfilt_crcf_create_sos(b,a,NUMBER_OF_SECTIONS);
      nco = nco_crcf_create(LIQUID_NCO);
      float normalized_freq = (float) tft / (float) Fs; 
      nco_crcf_set_frequency(nco,2*M_PI*normalized_freq);
      std::complex<float> y(0,0);
      float a;
      float b;
      for(int i =0; i < 40; i++)
      {
        nco_crcf_step(nco);
        nco_crcf_cexpf(nco,&y);
        nco_crcf_sincos(nco,&a,&b);
        if(i==23)
            nco_crcf_set_frequency(nco,2*M_PI/20);
        std::cout << a << " | "<<  b << std::endl;
      }
  return 0;
  }



