import socket
import message_pb2
UDP_IP = ""
UDP_PORT = 8898


print "UDP target IP:", UDP_IP
print "UDP target port", UDP_PORT

com =  message_pb2.DSPResponse()
com.azimuth_radians = 1.0;
com.zenith_radians = 1.1;
com.trigger_psd = 1.2;
com.frequency = 1.3;
print len(com.SerializeToString())

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP,UDP_PORT))

while 1:
    s,addr = sock.recvfrom(82)

    com.ParseFromString(s[0:20])
    print com
    print com.frequency
