%#!/usr/bin/octave -qf
clear
%arg_list = argv ();
%filename = arg_list{1};
filename='gain16pool.mat'
load(filename);
t = (1:length(ping_A))./400e3;

ping_A = double(ping_A) - mean(double(ping_A));
ping_B = double(ping_B) - mean(double(ping_B));
ping_C = double(ping_C) - mean(double(ping_C));
conf_factor = 3.3/4096;
figure(1);
plot(t,double(ping_A)*conf_factor,t,double(ping_B)*conf_factor); %,t,double(ping_C)*conf_factor);
%plot(t,ping_A,t,ping_B,t,ping_C);

%ylim([-.5 3.5])
legend('Transducer A', 'Transducer B', 'Transducer C');
title('Conditioned Transducer output vs Time','FontSize',16)
ylabel('V_{meas}','FontSize',16)
xlabel('Time (seconds)','FontSize',16);
set(gca,'FontSize',16)
%xlim([0,1e-3]);
%pause
figure(2);



%plot(t,fftshift(fft(double(ping_C)*conf_factor)))%,t,fftshift(fft(double(ping_B)*conf_factor)),t,fftshift(fft(double(ping_C)*conf_factor));
pwelch(ping_C);
